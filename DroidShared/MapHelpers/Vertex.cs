﻿using System;


namespace DroidShared
{
	public abstract class Vertex
	{
		public abstract string getID();
		public abstract string getX();
		public abstract string getY();
		public abstract string getFloorID();
		public abstract IBeacon getIBeacon();
	}
}

