﻿using System;
using System.Collections.Generic;
using Doppler.Droid;

namespace DroidShared
{
	public class ActiveStoryline
	{

		public Storyline storyline {get; set;}
		public List<Vertex> visitedPOIs {get; set;}
		public List<Vertex> unvisitedPOIs {get; set;}
		public List<string> traversedPoints {get; set;}
		public List<string> untraversedPoints {get; set;}

		public ActiveStoryline (Storyline s){
			storyline = s;
			untraversedPoints = s.path;
			traversedPoints = new List<string> ();
			unvisitedPOIs = new List<Vertex> ();
			visitedPOIs = new List<Vertex> ();
		}
			
		public void GenerateUnvisitedPOIs(List<Vertex> vertexPath){
			foreach(Vertex v in vertexPath){
				if (v is Poi) {
					unvisitedPOIs.Add (v);
				}
			}
		}

		//This returns the next POI on the storyline
		public Vertex GetCurrentUnvisitedPOI(){
			return unvisitedPOIs.Count > 0 ? unvisitedPOIs [0] : null;
		}

		public void UpdatePoiStates(){
			UpdatePointStates ();
			if (unvisitedPOIs.Count > 0) {
				visitedPOIs.Add (unvisitedPOIs [0]);
				unvisitedPOIs.RemoveAt (0);
			}
		}

		void UpdatePointStates(){
			
			List<string> tempTraversed = new List<string> ();
			List<string> tempUntraversed = new List<string> ();
			bool foundCurrentPoi = false;

			foreach (string point in storyline.path) {
				if (point == unvisitedPOIs [0].getID ()) {
					foundCurrentPoi = true;
					tempTraversed.Add (point);
				} 
					
				if (foundCurrentPoi && !tempTraversed.Contains (point) ) {
					tempUntraversed.Add (point);
				} else if (!tempTraversed.Contains (point))
					tempTraversed.Add (point);
			}

			traversedPoints = tempTraversed;
			untraversedPoints = tempUntraversed;
		}


	}
}

