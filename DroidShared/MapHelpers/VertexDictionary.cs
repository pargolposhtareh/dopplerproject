﻿using System;
using System.Collections.Generic;

namespace DroidShared
{
	public class VertexDictionary
	{
		static VertexDictionary instance = null;
		public Dictionary<string, Vertex> vertexDictionary { get; set; }

		VertexDictionary ()
		{
			vertexDictionary = new Dictionary<string,Vertex> ();
		}

		public static VertexDictionary GetInstance()
		{
			if (instance == null) 
			{
				instance = new VertexDictionary ();
			}
			return instance;
		}

		public void generateDictionary(List<Poi> pois, List<Pot> pots)
		{
			foreach (Poi poi in pois)
			{
				vertexDictionary.Add (poi.id, poi);
			}

			foreach (Pot pot in pots) 
			{
				vertexDictionary.Add (pot.id, pot);
			}
				
		}

		public Vertex GetVertexAtId(string id)
		{
			return vertexDictionary [id];
		}

		public List<Vertex> ListOfIdToListOfVertex(List<string> ids)
		{
			List<Vertex> vertices = new List<Vertex> ();

			foreach (String id in ids) 
			{
				vertices.Add (GetVertexAtId (id));
			}

			return vertices;
		}
			
	}
}

