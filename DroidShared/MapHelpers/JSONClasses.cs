﻿using System;
using System.Collections.Generic;

namespace DroidShared
{
	public class JSONClasses
	{
	}

	public class FloorPlan
	{
		public string floorID { get; set; }
		public string imagePath { get; set; }
		public string imageWidth { get; set; }
		public string imageHeight { get; set; }
	}

	public class Title
	{
		public string language { get; set; }
		public string title { get; set; }
	}

	public class Description
	{
		public string language { get; set; }
		public string description { get; set; }
	}

	public class IBeacon
	{
		public string uuid { get; set; }
		public string major { get; set; }
		public string minor { get; set; }
	}

	public class Image
	{
		public string path { get; set; }
		public string language { get; set; }
		public string caption { get; set; }
	}

	public class Video
	{
		public string path { get; set; }
		public string language { get; set; }
		public string caption { get; set; }
	}

	public class Audio
	{
		public string path { get; set; }
		public string language { get; set; }
		public string caption { get; set; }
	}

	public class Media
	{
		public List<Image> image { get; set; }
		public List<Video> video { get; set; }
		public List<Audio> audio { get; set; }
	}

	public class Title2
	{
		public string language { get; set; }
		public string title { get; set; }
	}

	public class Description2
	{
		public string language { get; set; }
		public string description { get; set; }
	}

	public class Image2
	{
		public string path { get; set; }
		public string language { get; set; }
		public string caption { get; set; }
	}

	public class Video2
	{
		public string path { get; set; }
		public string language { get; set; }
		public string caption { get; set; }
	}

	public class Audio2
	{
		public string path { get; set; }
		public string language { get; set; }
		public string caption { get; set; }
	}

	public class Media2
	{
		public List<Image2> image { get; set; }
		public List<Video2> video { get; set; }
		public List<Audio2> audio { get; set; }
	}

	public class StoryPoint
	{
		public string storylineID { get; set; }
		public List<Title2> title { get; set; }
		public List<Description2> description { get; set; }
		public Media2 media { get; set; }
	}

	public class Poi : Vertex
	{
		public string id;
		public List<Title> title { get; set; }
		public List<Description> description { get; set; }
		public string x;
		public string y;
		public string floorID;
		public IBeacon iBeacon { get; set; }
		public Media media { get; set; }
		public List<StoryPoint> storyPoint { get; set; }

		public override string getID(){ return id; }
		public override string getX(){ return x; }
		public override string getY(){ return y; }
		public override string getFloorID(){ return floorID; }
		public override IBeacon getIBeacon(){ return iBeacon; }
	}

	public class Label
	{
		public string language { get; set; }
		public string label { get; set; }
	}

	public class Pot : Vertex
	{
		public string id;
		public Label label { get; set; }
		public string x;
		public string y;
		public string floorID;

		public override string getID(){ return id; }
		public override string getX(){ return x; }
		public override string getY(){ return y; }
		public override string getFloorID(){ return floorID; }
		public override IBeacon getIBeacon() { return null; }
	}

	public class Node
	{
		public List<Poi> poi { get; set; }
		public List<Pot> pot { get; set; }
	}

	public class Edge
	{
		public string startNode { get; set; }
		public string endNode { get; set; }
		public string floorNumber { get; set; }
		public string distance { get; set; }
	}

	public class Title3
	{
		public string language { get; set; }
		public string title { get; set; }
	}

	public class Description3
	{
		public string language { get; set; }
		public string description { get; set; }
	}

	public class Storyline
	{
		public string id { get; set; }
		public List<Title3> title { get; set; }
		public List<Description3> description { get; set; }
		public List<string> path { get; set; }
		public string thumbnail { get; set; }
		public string walkingTimeInMinutes { get; set; }
		public string floorsCovered { get; set; }
	}

	public class RootObject
	{
		public List<FloorPlan> floorPlan { get; set; }
		public List<Node> node { get; set; }
		public List<Edge> edge { get; set; }
		public List<Storyline> storyline { get; set; }
	}

}


