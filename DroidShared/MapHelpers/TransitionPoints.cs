﻿using System;
using System.Collections.Generic;
using DroidShared;

namespace DroidShared
{
	public class TransitionPoints
	{
		static TransitionPoints instance = null;

		List<List<Pot>> exitFloorPots;
		readonly Pot[] washroomFloorPots;
		Pot restoFloorPot;
		Pot museumFloorPot;
		readonly MapGraph mg = MapGraph.GetInstance ();


		private TransitionPoints ()
		{
			// Assumption: each index corresponds to the floor where the washroom is located
			washroomFloorPots = new Pot[5];

			restoFloorPot = null;
			museumFloorPot = null;
		}

		public static TransitionPoints getInstance()
		{
			if (instance == null) {
				instance = new TransitionPoints ();
			}
			return instance;
		}

		public void intializelistPOTs(List<Pot> pots)
		{
			List<Pot> exitPots = new List<Pot> ();

			foreach (Pot p in pots) {

				switch (p.label.label)
				{
				case "washroom":
					int floorOfPot = Int32.Parse (p.getFloorID ());
					washroomFloorPots [floorOfPot - 1] = p;
					break;

				case "museum":
					museumFloorPot = p;
					break;

				case "exit":
				case "elevator":
				case "stairs":
					// need to exclude the exceptions
					exitPots.Add (p);
					break;

				// This is not part of the JSON file (see prof's class diagram)
				case "restaurant":
					restoFloorPot = p;
					break;

				default:
					break;
				}
			}

			exitFloorPots = splitExitListByFloor (exitPots);
		}

		private List<List<Pot>> splitExitListByFloor(List<Pot> pots)
		{
			List<List<Pot>> tempPotsFloor = new List<List<Pot>> ();
			List<Pot> tempFloor1 = new List<Pot> ();
			List<Pot> tempFloor2 = new List<Pot> ();
			List<Pot> tempFloor3 = new List<Pot> ();
			List<Pot> tempFloor4 = new List<Pot> ();
			List<Pot> tempFloor5 = new List<Pot> ();

			foreach (Pot p in pots) {

				switch (p.getFloorID()) {

				case "1":
					tempFloor1.Add (p);
					break;

				case "2":
					tempFloor2.Add (p);
					break;

				case "3":
					tempFloor3.Add (p);
					break;

				case "4":
					tempFloor4.Add (p);
					break;

				case "5":
					tempFloor5.Add (p);
					break;

				default:
					break;
				}
			}

			tempPotsFloor.Add (tempFloor1);
			tempPotsFloor.Add (tempFloor2);
			tempPotsFloor.Add (tempFloor3);
			tempPotsFloor.Add (tempFloor4);
			tempPotsFloor.Add (tempFloor5);

			return tempPotsFloor;
		}

		public List<Vertex> closestExit(Vertex userCurrentPosition)
		{
			int currentFloor = Int32.Parse(userCurrentPosition.getFloorID ());
			List<Pot> exit = exitFloorPots [currentFloor - 1];

			return getShortestPath (userCurrentPosition, exit);
		}

		public List<Vertex> closestPathMuseum(Vertex userCurrentPosition)
		{
			return mg.shortest_path (userCurrentPosition, museumFloorPot);
		}

		public List<Vertex> closestPathRestaurant(Vertex userCurrentPosition)
		{
			return mg.shortest_path (userCurrentPosition, restoFloorPot);
		}

		public List<Vertex> closestWashroom(Vertex userCurrentPosition)
		{
			int currentFloor = Int32.Parse(userCurrentPosition.getFloorID ());
			Vertex washroomVertex = washroomFloorPots [currentFloor - 1];

			return mg.shortest_path (userCurrentPosition, washroomVertex);
		}


		private List<Vertex> getShortestPath(Vertex userCurrentPosition, List<Pot> listPots)
		{
			VertexDictionary vd = VertexDictionary.GetInstance ();

			double shortestDistance = 99999; // exagerated
			List<Vertex> shortestPath = null;

			foreach (Pot p in listPots)
			{
				Vertex v = vd.GetVertexAtId (p.getID ());
				List<Vertex> tempPath = mg.shortest_path (userCurrentPosition, v);

				if (tempPath != null) {
					double tempDistance = getDistancePath (tempPath);
					if (shortestDistance > tempDistance) {
						shortestDistance = tempDistance;
						shortestPath = tempPath;
					}
				}
			}

			return shortestPath;
		}

		private double getDistancePath(List<Vertex> list)
		{
			double distance = 0;
			for (int i = 1; i < list.Count; i++) 
			{
				distance += mg.FindDistance (list [i-1], list [i]);
			}
			return distance;
		}
	}
}

