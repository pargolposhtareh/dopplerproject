﻿using System;
using System.Collections.Generic;
using System.Linq;
using DroidShared;

namespace DroidShared
{
	public class MapGraph
	{
		private static MapGraph instance = null;
		readonly Dictionary<Vertex, Dictionary<Vertex, double>> vertices = new Dictionary<Vertex, Dictionary<Vertex, double>>();

		private MapGraph() {}

		public static MapGraph GetInstance()
		{
			if (instance == null)
			{
				instance = new MapGraph ();
			}
			return instance;
		}

		public void add_vertex(Vertex node, Dictionary<Vertex, double> edges)
		{
			vertices[node] = edges;
		}

		//If n1 is a key of vertices, add pair <n2, distance> to its dictionary
		//else, create key n1 in vertices and give it a dictionary
		void add_edge (Vertex n1, Vertex n2, double distance)
		{
			if (vertices.ContainsKey (n1)) {
				vertices [n1].Add (n2, distance);
			}
			else {
				Dictionary<Vertex, double> e = new Dictionary<Vertex, double> ();
				e.Add (n2, distance);
				vertices.Add (n1, e);
			}
		}

		public void GenerateGraph(List<Edge> edges)
		{
			VertexDictionary nodeDictionary = VertexDictionary.GetInstance ();

			foreach (Edge edge in edges) 
			{
				Vertex startNode = nodeDictionary.vertexDictionary[edge.startNode];
				Vertex endNode = nodeDictionary.vertexDictionary[edge.endNode];
				double distance = FindDistance (startNode, endNode);
	
				add_edge (startNode, endNode, distance);
				add_edge (endNode, startNode, distance);
			}
		}

		public double FindDistance(Vertex startNode, Vertex endNode)
		{
			if (startNode == null || endNode == null) {
				return 0;
			}
			
			double startX = Double.Parse(startNode.getX());
			double startY = Double.Parse(startNode.getY());
			double endX = Double.Parse(endNode.getX());
			double endY = Double.Parse(endNode.getY());

			return Math.Sqrt (Math.Pow((startX - endX),2) + Math.Pow((startY - endY),2));
		}

		public bool Adjacent(Vertex a, Vertex b)
		{
			Dictionary<Vertex, double> neighbors = vertices[a];
			return neighbors.ContainsKey (b);
		}

		public List<Vertex> Neighbors(Vertex a)
		{
			List<Vertex> list = new List<Vertex>();
			Dictionary<Vertex, double> neighbors = vertices[a];
			foreach (KeyValuePair<Vertex, double> pair in neighbors) 
			{
				list.Add (pair.Key);
			}
			return list;
		}

		//Code from the internet. Forgot the source. Will try to find later!
		public List<Vertex> shortest_path(Vertex start, Vertex finish)
		{
			var previous = new Dictionary<Vertex, Vertex>();
			var distances = new Dictionary<Vertex, double>();
			var nodes = new List<Vertex>();

			List<Vertex> path = null;

			foreach (var vertex in vertices)
			{
				if (vertex.Key == start)
				{
					distances[vertex.Key] = 0;
				}
				else
				{
					distances [vertex.Key] = double.MaxValue;
				}

				nodes.Add(vertex.Key);
			}

			while (nodes.Count != 0)
			{
				double minDistance = double.MaxValue;
				Vertex smallest = nodes [0];
				foreach (Vertex node in nodes) {
					if (distances [node] < minDistance) 
					{
						minDistance = distances [node];
						smallest = node;
					}
				}
					
				nodes.Remove(smallest);

				if (smallest == finish)
				{
					path = new List<Vertex>();
					while (previous.ContainsKey(smallest))
					{
						path.Add(smallest);
						smallest = previous[smallest];
					}
					path.Add (start); //test
					break;
				}

				if (Math.Abs (distances [smallest] - int.MaxValue) < double.Epsilon) {
					break;
				}

				foreach (var neighbor in vertices[smallest])
				{
					var alt = distances[smallest] + neighbor.Value;
					if (alt < distances[neighbor.Key])
					{
						distances[neighbor.Key] = alt;
						previous[neighbor.Key] = smallest;
					}
				}
			}

			if (path != null) {
				path.Reverse ();
			}

			return path;
		}


	}
}

