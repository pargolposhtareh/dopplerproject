﻿
using System;
using DroidShared;

namespace DroidShared
{
	public class User
	{
		public Vertex position { get; set; }
		public ActiveStoryline activeStoryline { get; set; }

		public Vertex getVertex()
		{
			return position;
		}

		public void setPositionWithVertex (Vertex v)
		{
			this.position = v;
		}
	}
}

