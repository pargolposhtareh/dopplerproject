﻿using Android.Util;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using System.Collections.Generic;
using EstimoteSdk;
using Java.Util;
using DroidShared;


using Doppler.Droid;

namespace Doppler.Droid
{
	[Activity(Label = "@string/app_name", Icon = "@drawable/icon", Theme = "@android:style/Theme.Black.NoTitleBar.Fullscreen", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Landscape)]			
	public class MediaActivity : Activity
	{
		MediaController mediaController;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView (Resource.Layout.VideoLayout);

			mediaController = new MediaController(this);

			VideoView videoView = FindViewById<VideoView> (Resource.Id.vidView);
			Button closeVideo = FindViewById<Button> (Resource.Id.closeVid);

			string video = Intent.GetStringExtra ("VideoName") ?? "Video Not Found";

			MediaRepo mRepo = MediaRepo.GetInstance();

			var path = mRepo.getVideoPath(video);
			var uri = Android.Net.Uri.Parse (path);

			videoView.SetVideoURI(uri);
			videoView.SetMediaController(mediaController);

			videoView.Start ();

			closeVideo.Touch += (sender, e) => 
			{
				videoView.StopPlayback();
				Finish();
			};

			videoView.Completion += (sender, e) => 
			{
				Finish();
			};

		}
	}
}
