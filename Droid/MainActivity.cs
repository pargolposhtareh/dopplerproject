﻿
using System;
using Android.Util;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using System.Collections.Generic;
using EstimoteSdk;
using Java.Util;
using DroidShared;

using Doppler.Droid;

namespace Doppler.Droid
{
	[Activity(Label = "@string/app_name", Icon = "@drawable/icon", MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class SplashScreen : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			ISharedPreferences pref = Application.Context.GetSharedPreferences ("locale", FileCreationMode.Private);
			string myLocale = pref.GetString ("savedLocale", String.Empty);

			if (myLocale != String.Empty) {
				Locale lang = new Locale (myLocale);
				Java.Util.Locale.Default = lang;
				var config = new Android.Content.Res.Configuration { Locale = lang };
				Resources.UpdateConfiguration (config, Resources.DisplayMetrics);
			}
			StartActivity(typeof(MainActivity));
		}
	}

	[Activity(Label = "@string/app_name", Icon = "@drawable/icon", Theme="@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : ActionBarActivity, FragmentSettings
	{
		private SupportToolbar mToolbar;
		private ActionBarDrawerToggle mDrawerToggle;
		private DrawerLayout mDrawerLayout;
		private ListView mLeftDrawer;
		private ArrayAdapter mLeftAdapter;
		private List<string> mLeftDataSet;

		protected override void OnCreate (Bundle bundle)
		{

			base.OnCreate (bundle);

			SetContentView (Resource.Layout.main_activity);

			mToolbar = FindViewById<SupportToolbar> (Resource.Id.toolbar);
			mDrawerLayout = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			mLeftDrawer = FindViewById<ListView> (Resource.Id.left_drawer);

			SetSupportActionBar (mToolbar);

			mLeftDataSet = new List<string> ();
			mLeftDataSet.Add("Guided tour");
			mLeftDataSet.Add("Unguided tour");
			mLeftDataSet.Add("View Map");
			mLeftDataSet.Add("Settings");
			mLeftDataSet.Add("Help");
			mLeftDataSet.Add("Exit");

			mLeftAdapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, mLeftDataSet);
			mLeftDrawer.Adapter = mLeftAdapter;

			mDrawerToggle = new ActionBarDrawerToggle(
				this, 			// Host Activity
				mDrawerLayout,	// DrawerLayout
				0,
				0
			);

			mDrawerLayout.SetDrawerListener (mDrawerToggle);
			SupportActionBar.SetHomeButtonEnabled (true);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			mDrawerToggle.SyncState ();

			if (bundle == null)
				fragmentHandler (new MainMenu ());

			LocalJSON.deserializeJSONString ();

			List<Poi> pois = LocalJSON.GetRootObject().node[0].poi;
			List<Pot> pots = LocalJSON.GetRootObject().node [0].pot;
			List<DroidShared.Edge> edges = LocalJSON.GetRootObject().edge;

			VertexDictionary vd = VertexDictionary.GetInstance ();
			vd.generateDictionary(pois, pots);

			MapGraph graph = MapGraph.GetInstance ();
			graph.GenerateGraph (edges);

			TransitionPoints tp = TransitionPoints.getInstance ();
			tp.intializelistPOTs (pots);

			// initializing the user instance and their starting position
			Vertex position = vd.GetVertexAtId ("E210");
			PositionedUser user = PositionedUser.GetInstance ();
			user.setPositionWithVertex (position);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			mDrawerToggle.OnOptionsItemSelected (item);
			return base.OnOptionsItemSelected (item);
		}

		public void fragmentHandler(Fragment fragment)
		{
			this.FragmentManager.BeginTransaction ().Replace (Resource.Id.fragment_container, fragment).AddToBackStack (null).Commit ();
		}

		public void popViewStoryline()
		{
			Fragment fragment = this.FragmentManager.FindFragmentById (Resource.Id.fragment_container);

			if (FragmentManager.BackStackEntryCount > 0 && fragment.GetType () == typeof(ViewStoryline)) {
				FragmentManager.PopBackStack ();
			}
		}

		public override void OnBackPressed ()
		{
			Fragment fragment = this.FragmentManager.FindFragmentById (Resource.Id.fragment_container);

			if (FragmentManager.BackStackEntryCount > 0 && fragment.GetType () != typeof(MainMenu)) {

				if (fragment.GetType () == typeof(ViewStoryline)) {
					((ViewStoryline)fragment).createExitDialog ();
				}
				else {
					FragmentManager.PopBackStack ();
				}
			} else {
				Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
			}
		}

	}
}
