﻿using System;
using DroidShared;

namespace Doppler.Droid
{
	public class PositionedUser : User
	{
		static PositionedUser instance;
		public ViewUpdater viewUpdater { get; set;}

		PositionedUser ()
		{
			activeStoryline = null;
			InitializePosition ();
		}

		//singleton
		public static PositionedUser GetInstance() {
			if (instance == null) {
				instance = new PositionedUser ();
			}
			return instance;
		}

		public void updateLocationOnMap() {

			if (position == null)
				Console.WriteLine ("position is null :( ");
			ViewBase.mFloorView.LoadUrl("javascript:updateLocation(' "+ position.getX()+
				" ',' "+ position.getY() + " ',' " + position.getFloorID() + "')");
		}

		public void UpdatePathOnMap()
		{
			viewUpdater.UpdateDrawnPath ();
		}

		void InitializePosition()
		{
			VertexDictionary vd = VertexDictionary.GetInstance ();
			Vertex museumRoom = vd.GetVertexAtId ("E210"); // initialize position at museumLocation
			position = museumRoom;
			Console.WriteLine ("LOCATION: " + position.getID ());
		}
	}
}

