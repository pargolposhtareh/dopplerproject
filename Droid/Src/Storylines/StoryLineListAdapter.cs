﻿using System;
using System.Text;
using System.Collections.Generic;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Doppler.Droid;
using DroidShared;

namespace Doppler.Droid
{
	public class StoryLineListAdapter : BaseAdapter<Storyline>
	{

		readonly List<Storyline> mItems;
		readonly Context mContext;
		Button infoButton;
		Button startButton;
		Activity activity;

		public StoryLineListAdapter (Context context, List<Storyline> items)
		{
			mItems = items;
			mContext = context;
		}


		public override long GetItemId (int position)
		{
			return position;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View row = convertView;
			activity = (Activity) mContext;

			if (row == null) {
				row = LayoutInflater.From (mContext).Inflate (Resource.Layout.storyline_row, null, false);

				startButton = row.FindViewById<Button> (Resource.Id.SLlistStartButton);

				startButton.Click += delegate {

					// starting a storyline based on the item position in the listview
					PositionedUser.GetInstance().activeStoryline = new ActiveStoryline(mItems[position]);
					activity.FragmentManager.BeginTransaction().Replace(Resource.Id.fragment_container, new ViewStoryline(position)).AddToBackStack (null).Commit ();
				};
			}

			TextView txtTitle = row.FindViewById<TextView> (Resource.Id.storyLine_Title);
			txtTitle.Text = mItems [position].title[0].title;

			infoButton = row.FindViewById<Button> (Resource.Id.SLlistInfoButton);
			infoButton.Click += (object sender, EventArgs e) => 
			{
				FragmentTransaction transaction = activity.FragmentManager.BeginTransaction ();
				DialogStoryLineInfo dialog = new DialogStoryLineInfo (mItems[position], position);
				dialog.SetStyle (DialogFragmentStyle.NoTitle, 0);
				dialog.Show(transaction, "dialog fragment");
			};

			return row;
		}

		public override int Count {
			get {
				return mItems.Count;
			}
		}

		public override Storyline this [int index] {
			get {
				return mItems[index];
			}
		}

		public override bool IsEnabled (int position)
		{
			return false;
		}
	}
}