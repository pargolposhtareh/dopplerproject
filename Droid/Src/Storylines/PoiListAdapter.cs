﻿using System;
using System.Text;
using System.Collections.Generic;

using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using Doppler.Droid;
using Android.Util;
using DroidShared;

namespace Doppler.Droid
{
	public class PoiListAdapter : BaseAdapter<Poi>
	{
		readonly List<Poi> mPOIItems;
		readonly Context mContext;
		Button startButton;
		Activity activity;
		readonly Dialog dialog;
		readonly ViewUnguided view;

		public PoiListAdapter (Context context, List<Poi> mPOIItems, Dialog dialog, ViewUnguided view)
		{
			this.mPOIItems = mPOIItems;
			mContext = context;
			this.dialog = dialog;
			this.view = view;
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View row = convertView;
			activity = (Activity) mContext;

			if (row == null) {
				row = LayoutInflater.From (mContext).Inflate (Resource.Layout.PoisAtBeacon_row, null, false);

				startButton = row.FindViewById<Button> (Resource.Id.PoisAtBeaconViewButton);

				startButton.Click += delegate {
					view.getShortestPath(mPOIItems[position].id);
					dialog.Dismiss();
				};
			}

			TextView txtTitle = row.FindViewById<TextView> (Resource.Id.PoisAtBeacon_Title);
			//need to add functionality to choose language
			txtTitle.Text = mPOIItems [position].title [0].title;

			return row;
		}

		public override int Count {
			get {
				return mPOIItems.Count;
			}
		}

		public override Poi this [int index] {
			get {
				return mPOIItems[index];
			}
		}

		public override bool IsEnabled (int position)
		{
			return false;
		}
	}
}

