﻿using System;
using Java.Lang;

namespace Doppler.Droid
{
	public class StoryLine
	{

		public string title { get; set; }
		public string description { get; set; }
		public string walkingTime { get; set; }
		public string floorsCovered { get; set; }

		public StoryLine ()
		{
		}
	}
}

