using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System.IO;
using System.Reflection;
using DroidShared;

namespace Doppler.Droid
{
	public static class LocalJSON
	{
		//Container object for the parsed JSON data
		static RootObject rootObject = null;
		const string jsonPath = "Doppler.Droid.Assets.t1.json";

		public static string getJSONString() 
		{
			string json = "";

			Assembly assembly = Assembly.GetExecutingAssembly ();
			string[] resources = assembly.GetManifestResourceNames ();

			foreach (string resource in resources) {
				if (resource.Equals (jsonPath, StringComparison.InvariantCulture)) {
					Stream stream = assembly.GetManifestResourceStream (resource);
					if (stream != null) {
						using (var reader = new System.IO.StreamReader (stream)) {
							json = reader.ReadToEnd ();
						}
					}
				}
			}
			return json;
		}

		public static RootObject GetRootObject(){
			if (rootObject == null) 
			{
				deserializeJSONString ();
			}
			return rootObject;
		}

		public static void deserializeJSONString()
		{
			rootObject = JsonConvert.DeserializeObject<RootObject> (getJSONString ());
		}
	
	}

}
