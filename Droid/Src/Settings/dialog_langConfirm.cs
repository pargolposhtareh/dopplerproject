﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Java.Util;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Doppler.Droid
{

	public class dialog_langConfirm : DialogFragment
	{
		TextView confirmTxt;
		Button yesBtn;
		Button noBtn;
		readonly string language;
		Locale myLocale;
		readonly Settings mSettings;

		public dialog_langConfirm(string language, Settings mSettings)
		{
			this.language = language;	
			this.mSettings = mSettings;
		}
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
		    base.OnCreateView (inflater, container, savedInstanceState);

			View view = inflater.Inflate (Resource.Layout.dialog_confirmLang, container, false);
			confirmTxt = view.FindViewById<TextView> (Resource.Id.confLangTxt);
			confirmTxt.Text += (" " + language + "?");

			yesBtn = view.FindViewById<Button> (Resource.Id.btnYes);
			noBtn = view.FindViewById<Button> (Resource.Id.btnNo);

			yesBtn.Click += (object sender, EventArgs e) => {
				if(language.Equals("Français")) {
					changeLocale("fr",view);
					mSettings.mLangText.Text = "Langue";
				}
				else if(language.Equals("English")) {
					changeLocale("en",view);
					mSettings.mLangText.Text = "Language";
				}
				this.Dismiss();
			};

			noBtn.Click += (object sender, EventArgs e) => {
				if(language.Equals("English")) mSettings.mSpinner.SetSelection (2);
				else mSettings.mSpinner.SetSelection (1);
				this.Dismiss();
			};
			return view;
		}

		private void savePref(){
			ISharedPreferences pref = Application.Context.GetSharedPreferences ("locale", FileCreationMode.Private);
			ISharedPreferencesEditor edit = pref.Edit ();
			if (myLocale != null) {
				edit.PutString ("savedLocale", myLocale.ToString ());
				edit.Apply ();
			}
		}

		private void changeLocale(string loc, View view)
		{
			myLocale = new Locale(loc);
			Java.Util.Locale.Default = myLocale;
			var config = new Android.Content.Res.Configuration { Locale = myLocale };
			view.Resources.UpdateConfiguration (config, view.Resources.DisplayMetrics);
			savePref();
		}

	}
}

