using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Util;

namespace Doppler.Droid
{
	public class Settings : Fragment
	{
		View root;
		public TextView mLangText { get; set; }
		public Spinner mSpinner { get; set; }

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			
			root = inflater.Inflate (Resource.Layout.settings, container, false);

			mSpinner = root.FindViewById<Spinner> (Resource.Id.spinner);

		if (Java.Util.Locale.Default.ToString ().Equals ("en")) {
				mSpinner.SetSelection (0);
			} else {
				mSpinner.SetSelection (1);
			}

			mSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (langChoice);
			var adapter = ArrayAdapter.CreateFromResource (
				root.Context, Resource.Array.lang_array, Android.Resource.Layout.SimpleSpinnerItem);
			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			mSpinner.Adapter = adapter;

			mLangText = root.FindViewById<TextView> (Resource.Id.langText);
			return root;
		}
		private void langChoice (object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;

			if (spinner.GetItemAtPosition (e.Position).Equals("Français") && mLangText.Text.Equals("Language")) {
				confirmLang ("Français");
			} 
			else if(spinner.GetItemAtPosition (e.Position).Equals("English")&& mLangText.Text.Equals("Langue")){
				confirmLang ("English");
			}
		
		}

		private void confirmLang(string lang){
			FragmentTransaction transaction = FragmentManager.BeginTransaction ();
			dialog_langConfirm langconfDialog = new dialog_langConfirm (lang,this);
			langconfDialog.SetStyle (DialogFragmentStyle.NoTitle, 0);
			langconfDialog.Show (transaction,"dialog fragment");
		}
			
	}
}
	
