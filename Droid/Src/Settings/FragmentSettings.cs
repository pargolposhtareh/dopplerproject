﻿using System;
using Android.App;

namespace Doppler.Droid
{
	public interface FragmentSettings
	{
		void fragmentHandler(Fragment fragment);
	}
}

