﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using DroidShared;

using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace Doppler.Droid
{
	public class MediaRepo
	{
		static MediaRepo instance = null;

		readonly int intro = Resource.Raw.intro;
		readonly int vid1 = Resource.Raw.vid1;
		readonly int vid2 = Resource.Raw.vid2;
		readonly int vid3 = Resource.Raw.vid3;
		readonly int vid4 = Resource.Raw.vid4;

		public Dictionary<string, int> videoDictionary;

		public MediaRepo()
		{
			videoDictionary = new Dictionary<string, int>();
		}

		public Dictionary<string, int> setVideos()
		{
			if(videoDictionary.Count == 0)
			{
				videoDictionary.Add("intro" , intro);
				videoDictionary.Add("vid1" , vid1);
				videoDictionary.Add("vid2" , vid2);
				videoDictionary.Add("vid3" , vid3);
				videoDictionary.Add("vid4" , vid4);
			}

			return videoDictionary;
		}

		public static MediaRepo GetInstance()
		{
			if (instance != null) {
				return instance;
			} else {
				instance = new MediaRepo ();
				return instance;
			}

		}

		public string getVideoPath(string vid)
		{
			setVideos ();
			int video = videoDictionary[vid];
			string path = "android.resource://"+"com.companyname.doppler" +"/"+video; 
			return path;
		}
	}
}


