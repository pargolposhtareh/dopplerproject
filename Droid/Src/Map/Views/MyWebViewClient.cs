﻿using System;
using Android.Webkit;
using System.Collections.Generic;

namespace Doppler.Droid
{
	public class MyWebViewClient : WebViewClient
	{
		Boolean loadingFinished = true;
		Boolean redirect = false;
		readonly List<string> floorPath;
		readonly ViewStoryline viewStoryline;

		public MyWebViewClient(ViewStoryline view, List<string> floorPath)
		{
			this.viewStoryline = view;
			this.floorPath = floorPath;
		}

		public override bool ShouldOverrideUrlLoading(WebView view, string url)
		{
			if (!loadingFinished)
			{
				redirect = true;
			}

			loadingFinished = false;
			view.LoadUrl(url);
			Console.WriteLine("Loading web view...");
			return true;
		}

		public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
		{
			base.OnPageStarted(view, url, favicon);
			loadingFinished = false;
		}

		public override void OnPageFinished(WebView view, string url)
		{
			if (!redirect)
			{
				loadingFinished = true;
			}

			if (loadingFinished && !redirect) 
			{
				Console.WriteLine("Finished Loading!");
				this.viewStoryline.drawPath (floorPath);
			}
			else
			{
				redirect = false;
			}
		}

	}
}

