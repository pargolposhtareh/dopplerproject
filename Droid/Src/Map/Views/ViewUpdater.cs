﻿using System;
using System.Collections.Generic;
using DroidShared;

namespace Doppler.Droid
{
	public class ViewUpdater
	{
		readonly ViewBase viewBase;

		public ViewUpdater (ViewBase vb)
		{
			viewBase = vb;
		}

		public void UpdateDrawnPath()
		{
			List<string> untraversedPoints = PositionedUser.GetInstance ().activeStoryline.untraversedPoints;
			List<Vertex> untraversedVertex = VertexDictionary.GetInstance ().ListOfIdToListOfVertex (untraversedPoints);
			viewBase.parsePathToFloors (untraversedVertex);
		}
	}
}

