﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

using Java.Lang;
using Doppler.Droid;

using Android.Webkit;

namespace Doppler.Droid
{
	public class ViewMap : ViewBase
	{
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			initializeVariables (inflater, container);

			// these buttons are not required for this fragment
			mDrawPOIsButton.Visibility = ViewStates.Gone;
			mListPOIsButton.Visibility = ViewStates.Gone;
			transitionButtonLayout.Visibility = ViewStates.Gone;

			mFloor5Button.Touch += (sender, e) => 
			{
				if (!mFloor5Button.Checked)
				{
					selectedButton(true,false,false,false,false);
					mFloorView.LoadUrl("javascript:setFloor(5)");
				}
			};

			mFloor4Button.Touch += (sender, e) => 
			{
				if (!mFloor4Button.Checked)
				{
					selectedButton(false,true,false,false,false);
					mFloorView.LoadUrl("javascript:setFloor(4)");
				}
			};

			mFloor3Button.Touch += (sender, e) => 
			{
				if (!mFloor3Button.Checked)
				{
					selectedButton(false,false,true,false,false);
					mFloorView.LoadUrl("javascript:setFloor(3)");
				}
			};

			mFloor2Button.Checked = true;
			mFloor2Button.Touch += (sender, e) => 
			{
				if (!mFloor2Button.Checked)
				{
					selectedButton(false,false,false,true,false);
					mFloorView.LoadUrl("javascript:setFloor(2)");
				}
			};

			mFloor1Button.Touch += (sender, e) => 
			{
				if (!mFloor1Button.Checked)
				{
					selectedButton(false,false,false,false,true);
					mFloorView.LoadUrl("javascript:setFloor(1)");
				}
			};

			return root;
		}
	}
}
