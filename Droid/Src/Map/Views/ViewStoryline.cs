﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Graphics;
using DroidShared;

namespace Doppler.Droid
{
	public class ViewStoryline : ViewBase
	{
		readonly int position;
		List<string> path;
		RangingManager rangingManager;
		ViewUpdater viewUpdater;

		public ViewStoryline(int position)
		{
			this.position = position;
		}

		//Give the User an Active Storyline
		//Create a Guided Tour beacon
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			user = PositionedUser.GetInstance ();
			rangingManager = new RangingManager (Activity, user.activeStoryline);
		}


		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			initializeVariables (inflater, container);
			viewUpdater = new ViewUpdater (this);
			user.viewUpdater = viewUpdater;

			path = LocalJSON.GetRootObject().storyline[position].path;

			// these buttons are not required for this fragment
			mDrawPOIsButton.Visibility = ViewStates.Gone;
			mListPOIsButton.Visibility = ViewStates.Gone;

			mFloor5Button.Touch += (sender, e) => 
			{
				currentFloor = 5;
				if (!mFloor5Button.Checked)
				{
					selectedButton(true,false,false,false,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(5)");
					drawPath (pathFloor5);
				}
			};

			mFloor4Button.Touch += (sender, e) => 
			{
				currentFloor = 4;
				if (!mFloor4Button.Checked)
				{
					selectedButton(false,true,false,false,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(4)");
					drawPath (pathFloor4);
				}
			};

			mFloor3Button.Touch += (sender, e) => 
			{
				currentFloor = 3;
				if (!mFloor3Button.Checked)
				{
					selectedButton(false,false,true,false,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(3)");
					drawPath (pathFloor3);
				}
			};

			mFloor2Button.Checked = true;
			mFloor2Button.Touch += (sender, e) => 
			{
				currentFloor = 2;
				if (!mFloor2Button.Checked)
				{
					selectedButton(false,false,false,true,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(2)");
					drawPath (pathFloor2);
				}
			};

			mFloor1Button.Touch += (sender, e) => 
			{
				currentFloor = 1;
				if (!mFloor1Button.Checked)
				{
					selectedButton(false,false,false,false,true);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(1)");
					drawPath (pathFloor1);
				}
			};

			parsePathToFloors (GenerateVertexPath(path));
			pathCurrentStoryline = GenerateVertexPath(path); // used to recall storyline path 

			// Webclient waits for the mapIndex file to load before drawing the path on it
			webViewClient = new MyWebViewClient (this, paths[currentFloor]);
			mFloorView.SetWebViewClient (webViewClient);

			recoverPOI.Click += delegate {

				Poi mostRecentPoi = rangingManager.recentPoi.mostRecentPoi();
				rangingManager.DisplayNextStoryLinePOI(mostRecentPoi, false);
			};

			return root;
		}

		public override void OnPause ()
		{
			base.OnPause ();
			rangingManager.beaconManager.StopRanging(RangingManager.region);
		}

		public override void OnResume ()
		{
			base.OnResume ();
			rangingManager.beaconManager.StartRanging (RangingManager.region);
		}

		private List<Vertex> GenerateVertexPath(List<string> stringPath) {
			VertexDictionary vd = VertexDictionary.GetInstance ();
			List<Vertex> shortTestPathFromUserLocToStoryStart = getShortestPath (stringPath [0]);
			List<Vertex> storyPath = vd.ListOfIdToListOfVertex (stringPath);
			List<Vertex> vertexPath = new List<Vertex>();

			foreach (Vertex v in shortTestPathFromUserLocToStoryStart) 
			{
				vertexPath.Add (v);
			}
			foreach (Vertex v in storyPath) 
			{
				vertexPath.Add (v);
			}
			return vertexPath;
		}

		public void createExitDialog()
		{
			FragmentTransaction transaction = FragmentManager.BeginTransaction ();
			DialogExitStoryline dialog = new DialogExitStoryline ();
			dialog.SetStyle (DialogFragmentStyle.NoTitle, 0);
			dialog.Show (transaction,"dialog fragment");
		}
			
		public List<Vertex> getShortestPath(string finishID)
		{
			Vertex start = PositionedUser.GetInstance().getVertex();
			Vertex finish = VertexDictionary.GetInstance ().GetVertexAtId (finishID);
			MapGraph graph = MapGraph.GetInstance ();
			List<Vertex> shortestPath = graph.shortest_path (start, finish);

			return shortestPath;
		}



	}
}

