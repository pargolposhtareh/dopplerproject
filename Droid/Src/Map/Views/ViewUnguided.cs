﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using DroidShared;

namespace Doppler.Droid
{
	public class ViewUnguided : ViewBase
	{
		readonly List<Poi> poiList = POIList.GetInstance().pois;
		RangingManager rangingManager;
		bool poisDisplayed = false;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			rangingManager = new RangingManager (this.Activity);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			initializeVariables (inflater, container);

			mFloor5Button.Touch += (sender, e) => 
			{
				currentFloor = 5;
				if (!mFloor5Button.Pressed)
				{
					selectedButton(true,false,false,false,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(5)");
					drawPath (pathFloor5);
				}
			};

			mFloor4Button.Touch += (sender, e) => 
			{
				currentFloor = 4;
				if (!mFloor4Button.Pressed)
				{
					selectedButton(false,true,false,false,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(4)");
					drawPath (pathFloor4);
				}
			};

			mFloor3Button.Touch += (sender, e) => 
			{
				currentFloor = 3;
				if (!mFloor3Button.Pressed)
				{
					selectedButton(false,false,true,false,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(3)");
					drawPath (pathFloor3);
				}
			};

			mFloor2Button.Checked = true;
			mFloor2Button.Touch += (sender, e) => 
			{
				currentFloor = 2;
				if (!mFloor2Button.Pressed)
				{
					selectedButton(false,false,false,true,false);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(2)");
					drawPath (pathFloor2);
				}
			};

			mFloor1Button.Touch += (sender, e) => 
			{
				currentFloor = 1;
				if (!mFloor1Button.Pressed)
				{
					selectedButton(false,false,false,false,true);
					unsetTransitions();
					mFloorView.LoadUrl("javascript:setFloor(1)");
					drawPath (pathFloor1);
				}
			};
				
			mDrawPOIsButton.Click += delegate
			{
				if(poisDisplayed)
				{
					poisDisplayed = false;
					mFloorView.LoadUrl("javascript:removeAllPOIMarkers()");
				}
				else
				{
					poisDisplayed = true;
					foreach (Poi poi in poiList) 
					{
						mFloorView.LoadUrl ("javascript:AddPOItoLayerGroup(" + poi.x + "," + poi.y + "," + poi.floorID + ")");
					}
					mFloorView.LoadUrl("javascript:drawPOIs()");
				}
			};

			mListPOIsButton.Click += delegate
			{
				listOfPOIDialog();
			};

			recoverPOI.Click += delegate {

				Poi mostRecentPoi = rangingManager.recentPoi.mostRecentPoi();
				rangingManager.DisplayNextPOI(mostRecentPoi, false);
			};

			return root;
		}

		public override void OnPause ()
		{
			Console.WriteLine ("12345 OnPause should stop scanning");
			base.OnPause ();
			rangingManager.beaconManager.StopRanging (RangingManager.region);
		}

		public override void OnResume ()
		{
			Console.WriteLine ("12345 OnResume should start scanning");
			base.OnResume ();
			rangingManager.beaconManager.StartRanging (RangingManager.region);
		}

		public void listOfPOIDialog()
		{
			FragmentTransaction transaction = FragmentManager.BeginTransaction ();

			DialogListOfPoisAtBeacon listPOI = new DialogListOfPoisAtBeacon (poiList, this);
			listPOI.SetStyle (DialogFragmentStyle.NoTitle, 0);
			listPOI.Show (transaction,"dialog fragment");
		}


		public void getShortestPath(string finishID)
		{
			Vertex start = PositionedUser.GetInstance().getVertex();
			Vertex finish = VertexDictionary.GetInstance ().GetVertexAtId (finishID);
			MapGraph graph = MapGraph.GetInstance ();
			List<Vertex> shortestPath = graph.shortest_path (start, finish);

			if (shortestPath != null)
			{
				parsePathToFloors (shortestPath);
			}
			else
			{
				Toast.MakeText (root.Context, "No path exists from your current location to " + finish.getID(), ToastLength.Short).Show();
			}
		}
	}
}

