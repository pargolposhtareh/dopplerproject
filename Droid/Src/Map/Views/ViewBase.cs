﻿using System;
using Android.Widget;
using Android.Webkit;
using Android.Views;
using Android.App;
using System.Collections.Generic;
using DroidShared;
using System.Text;
using Android.Util;

namespace Doppler.Droid
{
	public abstract class ViewBase : Fragment
	{
		
		protected PositionedUser user;
		protected ToggleButton mFloor5Button;
		protected ToggleButton mFloor4Button;
		protected ToggleButton mFloor3Button;
		protected ToggleButton mFloor2Button;
		protected ToggleButton mFloor1Button;
		protected ToggleButton restoButton;
		protected ToggleButton washroomButton;
		protected ToggleButton exitButton;
		protected ToggleButton museumButton;
		protected ToggleButton[] buttonArray;
		protected Button mDrawPOIsButton;
		protected Button mListPOIsButton;
		public static WebView mFloorView;
		protected View root;
		protected int currentFloor = 2;
		protected MyWebViewClient webViewClient;
		protected LinearLayout transitionButtonLayout;
		protected TransitionPoints tp = TransitionPoints.getInstance();
		protected List<string> pathFloor1 = new List<string> ();
		protected List<string> pathFloor2 = new List<string> ();
		protected List<string> pathFloor3 = new List<string> ();
		protected List<string> pathFloor4 = new List<string> ();
		protected List<string> pathFloor5 = new List<string> ();
		protected List<Vertex> pathCurrentStoryline;
		protected Dictionary<int, List<string>> paths = new Dictionary<int, List<string>> ();
		protected VertexDictionary nd = VertexDictionary.GetInstance ();

		protected Button recoverPOI;

		protected void initializeVariables(LayoutInflater inflater, ViewGroup container)
		{
			root = inflater.Inflate (Resource.Layout.map_display, container, false);

			mDrawPOIsButton = root.FindViewById<Button> (Resource.Id.drawPOI);
			mListPOIsButton = root.FindViewById<Button> (Resource.Id.listPOI);

			mFloorView = root.FindViewById<WebView>(Resource.Id.floor_view);

			mFloorView.SetWebChromeClient (new WebChromeClient ());
			mFloorView.Settings.DefaultZoom = WebSettings.ZoomDensity.Far;
			mFloorView.Settings.JavaScriptEnabled = true;
			mFloorView.LoadUrl("file:///android_asset/mapIndex.html");
			mFloorView.LoadUrl("javascript:setFloor(" + currentFloor + ")");

			mFloor5Button = root.FindViewById<ToggleButton> (Resource.Id.buttonFloor5);
			mFloor4Button = root.FindViewById<ToggleButton> (Resource.Id.buttonFloor4);
			mFloor3Button = root.FindViewById<ToggleButton> (Resource.Id.buttonFloor3);
			mFloor2Button = root.FindViewById<ToggleButton> (Resource.Id.buttonFloor2);
			mFloor1Button = root.FindViewById<ToggleButton> (Resource.Id.buttonFloor1);

			user = PositionedUser.GetInstance ();

			recoverPOI = root.FindViewById<Button> (Resource.Id.recoverPOI);

			buttonArray = new ToggleButton[]{mFloor1Button, mFloor2Button, mFloor3Button, mFloor4Button, mFloor5Button};

			transitionButtonLayout = root.FindViewById<LinearLayout> (Resource.Id.includeClosestTransition);
			restoButton = root.FindViewById<ToggleButton> (Resource.Id.restoButton);
			washroomButton = root.FindViewById<ToggleButton> (Resource.Id.washroomButton);
			exitButton = root.FindViewById<ToggleButton> (Resource.Id.exitButton);
			museumButton = root.FindViewById<ToggleButton> (Resource.Id.museumButton);

			restoButton.Click += (o, e) => 
			{
				if (restoButton.Checked)
				{
					unsetWashroomExit();
					selectedButtonTransition(true,false,false,false);

					List<Vertex> restoPath = tp.closestPathRestaurant(user.getVertex());

					parsePathToFloors(restoPath);
				}
				else
				{
					resetStoryline();
				}
			};

			washroomButton.Click += (o, e) => 
			{
				if (washroomButton.Checked)
				{
					unsetMuseumResto();
					selectedButtonTransition(false,true,false,false);
					drawPathTransition(tp.closestWashroom(user.getVertex()));
				}
				else
				{
					washroomButton.Checked = false;
					mFloorView.LoadUrl ("javascript:resetTransitionDrawings()");
					resetStoryline();
				}
			};

			exitButton.Click += (o, e) => 
			{
				if (exitButton.Checked)
				{
					unsetMuseumResto();
					selectedButtonTransition(false,false,true,false);
					drawPathTransition(tp.closestExit(user.getVertex()));
				}
				else
				{
					exitButton.Checked = false;
					mFloorView.LoadUrl ("javascript:resetTransitionDrawings()");
					resetStoryline();
				}
			};

			museumButton.Click += (o, e) => 
			{
				if (museumButton.Checked)
				{
					unsetWashroomExit();
					selectedButtonTransition(false,false,false,true);

					List<Vertex> museumPath = tp.closestPathMuseum(user.getVertex());

					parsePathToFloors(museumPath);
				}
				else
				{
					resetStoryline();
				}
	
			};

			paths.Add (1, pathFloor1);
			paths.Add (2, pathFloor2);
			paths.Add (3, pathFloor3);
			paths.Add (4, pathFloor4);
			paths.Add (5, pathFloor5);
		}

		public void selectedButton(bool floor5, bool floor4, bool floor3, bool floor2, bool floor1)
		{
			mFloor5Button.Checked = floor5;
			mFloor4Button.Checked = floor4;
			mFloor3Button.Checked = floor3;
			mFloor2Button.Checked = floor2;
			mFloor1Button.Checked = floor1;
		}

		public void selectedButtonTransition(bool resto, bool washroom, bool exit, bool museum)
		{
			restoButton.Checked = resto;
			washroomButton.Checked = washroom;
			exitButton.Checked = exit;
			museumButton.Checked = museum;
		}

		public void drawPath(List<string> floorPath)
		{
			StringBuilder sb = new StringBuilder ();

			foreach (string s in floorPath)
			{
				if (!(s.Contains ("99999"))) {
					Vertex v = nd.GetVertexAtId (s);
					sb.Append (v.getX ());
					sb.Append (":");
					sb.Append (v.getY ());
					sb.Append (":");
				} else {
					sb.Append (s);
				}
			}

			string pathToDraw = sb.ToString ();

			mFloorView.LoadUrl ("javascript:process('" + pathToDraw + "')");
		}

		public void drawPathTransition(List<Vertex> floorPath)
		{
			StringBuilder sb = new StringBuilder ();

			if (floorPath != null) {
				foreach (Vertex v in floorPath) {
					sb.Append (v.getX ());
					sb.Append (":");
					sb.Append (v.getY ());
					sb.Append (":");
				}
			}
				
			string pathToDraw = sb.ToString ();

			int floor = Int32.Parse(user.getVertex ().getFloorID ());

			// only display path when on the current floor 
			if (buttonArray [floor - 1].Checked) {
				mFloorView.LoadUrl ("javascript:processTransition('" + pathToDraw + "')");
			}
		}

		public void parsePathToFloors(List<Vertex> path)
		{
			//remove previous paths
			foreach (KeyValuePair <int, List<string>> pair in paths) 
			{
				pair.Value.Clear ();
			}
			int nextVertexFloor = 2, currentVertexFloor;

			for(int i = 0; i < path.Count; i++)
			{
				if(i+1 < path.Count)
				{
					nextVertexFloor = Int32.Parse(path [i + 1].getFloorID ());
				}

				currentVertexFloor = Int32.Parse (path [i].getFloorID ());
				paths [currentVertexFloor].Add (path [i].getID());

				if (nextVertexFloor > currentVertexFloor) 
				{
					paths [currentVertexFloor].Add ("99999:"); //going up
				}
				else if (nextVertexFloor < currentVertexFloor)  
				{
					paths [currentVertexFloor].Add ("-99999:"); //going down
				}
			}

			drawPath (paths[currentFloor]);
		}
			

		protected void unsetTransitions()
		{
			selectedButtonTransition (false, false, false, false);
		}

		private void unsetMuseumResto()
		{
			restoButton.Checked = false;
			museumButton.Checked = false;
			mFloorView.LoadUrl ("javascript:process('" + "" + "')");
		}

		private void unsetWashroomExit()
		{
			washroomButton.Checked = false;
			exitButton.Checked = false;
			mFloorView.LoadUrl ("javascript:resetTransitionDrawings()");
		}

		private void resetStoryline()
		{
			if (pathCurrentStoryline != null) {
				parsePathToFloors (pathCurrentStoryline);
			} else {
				mFloorView.LoadUrl ("javascript:process('" + "" + "')");
			}
		}
	}
}

