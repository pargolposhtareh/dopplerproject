﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Doppler.Droid;
using Android.Widget;
using DroidShared;

namespace Doppler.Droid
{
	public class StorylineMenu : Fragment
	{

		private List<Storyline> mStoryLines;
		private ListView mListView;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View root = inflater.Inflate (Resource.Layout.storyline_listview, container, false);
			mListView = root.FindViewById<ListView> (Resource.Id.storyline_listview);

			mListView.Focusable = false;
			mStoryLines = new List<Storyline> ();
			for (int i = 0; i < LocalJSON.GetRootObject().storyline.Count; i++) {
				mStoryLines.Add (LocalJSON.GetRootObject().storyline.ElementAt (i));
			}


			StoryLineListAdapter adapter = new StoryLineListAdapter (this.Activity, mStoryLines);
			mListView.Adapter = adapter;

			return root;
		}

	}
}