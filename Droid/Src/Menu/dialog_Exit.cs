﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Java.Util;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Doppler.Droid
{
	

	public class dialog_Exit : DialogFragment
	{
		private Button yesBtn;
		private Button noBtn;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment

		    base.OnCreateView (inflater, container, savedInstanceState);

			var view = inflater.Inflate (Resource.Layout.dialog_confirmExit, container, false);

			yesBtn = view.FindViewById<Button> (Resource.Id.btnYes);
			noBtn = view.FindViewById<Button> (Resource.Id.btnNo);

			yesBtn.Click += (object sender, EventArgs e) => {
				
				Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
				this.Dismiss();
			};

			noBtn.Click += (object sender, EventArgs e) => {
				
				this.Dismiss();
			};
			return view;
		}

	}
}

