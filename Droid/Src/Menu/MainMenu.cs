﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Doppler.Droid
{
	public class MainMenu : Fragment
	{
		private Button mGuidedTourButton;
		private Button mUnguidedTourButton;
		private Button mSettingsButton;
		private Button mExitButton;
		private Button mViewMapButton;
		private Button mHelpButton;
		private FragmentSettings mFragmentSettings;

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment

			View root = inflater.Inflate (Resource.Layout.main_menu, container, false);

			mFragmentSettings = (FragmentSettings) Activity;

			mGuidedTourButton = root.FindViewById<Button> (Resource.Id.buttonGuidedTour);
			mGuidedTourButton.Click += delegate {
				mFragmentSettings.fragmentHandler (new StorylineMenu ());
			};

			mUnguidedTourButton = root.FindViewById<Button> (Resource.Id.buttonUnguidedTour);
			mUnguidedTourButton.Click += delegate {
				mFragmentSettings.fragmentHandler (new ViewUnguided ());
			};

			mSettingsButton = root.FindViewById<Button> (Resource.Id.buttonSettings);
			mSettingsButton.Click += delegate {
				mFragmentSettings.fragmentHandler (new Settings ());
			};
				
			mExitButton = root.FindViewById<Button> (Resource.Id.buttonExit);
			mExitButton.Click += delegate {
				confirmExit();
			};

			mViewMapButton = root.FindViewById<Button> (Resource.Id.buttonViewMap);
			mViewMapButton.Click += delegate {
				mFragmentSettings.fragmentHandler (new ViewMap ());
			};

			mHelpButton = root.FindViewById<Button> (Resource.Id.buttonHelp);
			mHelpButton.Click += delegate {
				mFragmentSettings.fragmentHandler (new Help ());
			};

			return root;
		}
		public void confirmExit(){
			FragmentTransaction transaction = FragmentManager.BeginTransaction ();
			dialog_Exit exitDialog = new dialog_Exit ();
			exitDialog.SetStyle (DialogFragmentStyle.NoTitle, 0);
			exitDialog.Show (transaction,"dialog fragment");
		}

	}


}

