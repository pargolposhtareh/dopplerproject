﻿using System;
using SupportActionBarDrawerToggle = Android.Support.V7.App.ActionBarDrawerToggle;
using Android.Support.V7.App;
using Android.Support.V4.Widget;

namespace Doppler.Droid
{
	public class ActionBarDrawerToggle : SupportActionBarDrawerToggle
	{
		readonly ActionBarActivity mHostActivity;

		public ActionBarDrawerToggle (ActionBarActivity host, DrawerLayout drawerLayout, int open, int closed) : base(host, drawerLayout, open, closed)
		{
			mHostActivity = host;
		}

		public override void OnDrawerOpened (Android.Views.View drawerView)
		{
			base.OnDrawerOpened (drawerView);
		}

		public override void OnDrawerClosed (Android.Views.View drawerView)
		{
			base.OnDrawerClosed (drawerView);
		}

		public override void OnDrawerSlide (Android.Views.View drawerView, float slideOffset)
		{
			base.OnDrawerSlide (drawerView, slideOffset);
		}
	}
}

