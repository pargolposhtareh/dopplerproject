﻿using System;
using System.Collections.Generic;
using DroidShared;

namespace Doppler.Droid
{
	public class RecentPoi
	{
		List<Poi> recentPois;

		public RecentPoi ()
		{
			recentPois = new List<Poi> ();
		}

		public void addPoiToList(Poi poi)
		{
			if(!recentPois.Contains(poi)){
				recentPois.Add (poi);
				if (recentPois.Count > 2) {
					removeOldestVisitedFromList ();
				}
			}
		}

		public void removePoiFromList(Poi poi)
		{
			recentPois.Remove (poi);
		}

		public bool PoiRecentlyVisited(Poi poi)
		{
			return recentPois.Contains (poi);
		}

		public void removeOldestVisitedFromList()
		{
			recentPois.Remove (recentPois [0]);
		}

		public Poi mostRecentPoi()
		{
			return recentPois [recentPois.Count - 1];
		}

		public bool isEmpty()
		{
			int size = recentPois.Count;

			if (size == 0)
				return true;
			else
				return false;
		}



	}
}

