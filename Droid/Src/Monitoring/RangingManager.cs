using System;
using Android.App;
using Android.Bluetooth;

using EstimoteSdk;
using Doppler.Droid;
using Android.Widget;
using Android.Runtime;
using Android.Content;
using Android.Views;
using Java.Util.Concurrent;
using System.Collections.Generic;
using DroidShared;

namespace Doppler.Droid
{
	public class RangingManager : Java.Lang.Object, BeaconManager.IServiceReadyCallback, BeaconManager.IRangingListener
	{
		public static readonly Region region = new Region("test", "B9407F30-F5F8-466E-AFF9-25556B57FE6D");
		readonly Activity activity;
		public BeaconManager beaconManager { get; set;}
		public List<Vertex> storyLinePath;
		public RecentPoi recentPoi;
		readonly Boolean hasStoryline;
		readonly ActiveStoryline activeStoryline;
		readonly PositionedUser user = PositionedUser.GetInstance ();

		//For Unguided Tours
		public RangingManager(Activity activity)
		{
			this.recentPoi = new RecentPoi ();
			this.activity = activity;
			beaconManager = new BeaconManager (activity);
			beaconManager.SetRangingListener (this);
			beaconManager.Connect (this);
		}

		//Four Guided Tours
		public RangingManager(Activity activity, ActiveStoryline activeStoryline)
		{
			this.recentPoi = new RecentPoi ();
			this.activity = activity;
			hasStoryline = true;
			beaconManager = new BeaconManager (activity);
			beaconManager.Connect (this);
			beaconManager.SetRangingListener (this);

			//Give the user the Active Storyline
			this.activeStoryline = activeStoryline;
			initializeActiveStoryLine ();
		}

		void initializeActiveStoryLine()
		{
			List<string> stringPath = activeStoryline.untraversedPoints;
			activeStoryline.GenerateUnvisitedPOIs(VertexDictionary.GetInstance().ListOfIdToListOfVertex(stringPath));
		}

		void DisplayPOIDiaolg (Poi poi, bool trigger)
		{
			FragmentTransaction transaction = activity.FragmentManager.BeginTransaction ();
			DialogNearbyPOINotification dialog = new DialogNearbyPOINotification (poi, beaconManager, trigger);
			dialog.SetStyle (DialogFragmentStyle.NoTitle, 0);
			dialog.Show (transaction, "dialog fragment");
		}
			
		public void OnServiceReady ()
		{
			beaconManager.StartRanging (region);
		}

		void UpdateUserPosition (Poi currentPoi)
		{
			user.setPositionWithVertex (currentPoi);
			user.updateLocationOnMap ();
		}

		Poi GetPOIatBeacon (Beacon beacon)
		{
			Poi currentPoi = null;
			foreach (Poi poi in POIList.GetInstance ().pois) {
				if (poi.iBeacon.major == beacon.Major.ToString () && poi.iBeacon.minor == beacon.Minor.ToString ()) {
					user.setPositionWithVertex (poi);
					currentPoi = poi;
				}
			}
			return currentPoi;
		}

		public void DisplayNextPOI (Poi currentPoi, bool trigger)
		{
			Console.WriteLine ("12345 " + currentPoi.getID ());
			recentPoi.addPoiToList (currentPoi);
			Console.WriteLine ("12345 " + recentPoi.mostRecentPoi().getID());
			DisplayPOIDiaolg (currentPoi, trigger);
		}

		public void DisplayNextStoryLinePOI(Poi currentPoi, bool trigger)
		{
			if(currentPoi.Equals(activeStoryline.unvisitedPOIs[0]))
			{
				recentPoi.addPoiToList (currentPoi);
				DisplayPOIDiaolg (currentPoi, trigger);
			}
		}

		public void OnBeaconsDiscovered (Region region, IList<Beacon> beacons)
		{
			if (beacons.Count > 0) {
				Poi currentPoi = GetPOIatBeacon (beacons [0]);
				UpdateUserPosition (currentPoi);
				if(CheckIfBeaconIsNear(beacons[0]) && !recentPoi.PoiRecentlyVisited(currentPoi))
				{
					beaconManager.StopRanging (region);
					if (!hasStoryline) {
						DisplayNextPOI (currentPoi, true);
					} else {
						DisplayNextStoryLinePOI (currentPoi, true);
						user.UpdatePathOnMap ();
					}
				}
			} else {
				Console.WriteLine ("size is 0");
			}
				
		}

		public bool CheckIfBeaconIsNear(Beacon beacon)
		{
			double beaconDistance = Utils.ComputeAccuracy (beacon);
			if (beaconDistance > 0 && beaconDistance < 5)
				return true;
			else
				return false;
		}

	}
}
