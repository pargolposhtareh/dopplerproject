﻿using System;
using System.Collections.Generic;
using Doppler.Droid;
using EstimoteSdk;
using DroidShared;

namespace Doppler.Droid
{
	public class POIList
	{
		static POIList instance = null;
		public List<Poi> pois;

		public POIList ()
		{
			pois = new List<Poi> ();
			populateListFromRootObject ();
		}

		public static POIList GetInstance() {
			if (instance == null)
				instance = new POIList ();
			return instance;
		}

		public Poi getPoiFromVertex(Vertex v)
		{
			foreach(Poi p in pois)
			{
				if (v.getID () == p.getID ()) 
				{
					return p;
				}
			}

			return null;
		}

		private void populateListFromRootObject() 
		{
			foreach (Poi poi in LocalJSON.GetRootObject().node[0].poi) 
			{
				pois.Add (poi);
			}
		}

		public List<Poi> findPoisAtBeacons(IList<Beacon> beacons)
		{
			List<Poi> poisAtBeacon = new List<Poi> ();
			List<string> major = new List<string>();
			List<string> minor = new List<string>();

			foreach (Beacon beacon in beacons) 
			{
				major.Add (beacon.Major.ToString ());
				minor.Add (beacon.Minor.ToString ());
			}

			foreach (Poi poi in pois) 
			{
				if (major.Contains(poi.iBeacon.major) && minor.Contains(poi.iBeacon.minor))
				{
					poisAtBeacon.Add (poi);
				}
			}
			return poisAtBeacon;
		}
}
}


