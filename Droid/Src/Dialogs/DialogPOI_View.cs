﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Util;
using DroidShared;
using EstimoteSdk;

namespace Doppler.Droid
{
	public class DialogPOI_View: DialogFragment
	{
		Storyline storyline;
		BeaconManager beaconManger;
		readonly Poi poi;
		readonly Locale locale = Java.Util.Locale.Default; //EN_CA
		string language = "FR";
		string mediaPath = null;

		Button closeButton;
		Button nextButton;
		Button playButton;
		TextView storylineTitle;
		TextView poiTitle;
		TextView poiPosition;
		TextView poiDescription;
		TextView mediaType;
		TextView mediaCaption;
		LinearLayout tourLinearLayout;
		LinearLayout positionLinearLayout;
		LinearLayout mediaLinearLayout;


		public DialogPOI_View(Poi poi, BeaconManager bm)
		{
			this.poi = poi;
			this.beaconManger = bm;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);

			View root = inflater.Inflate (Resource.Layout.dialog_POIinfo, container, false);

			storylineTitle = root.FindViewById<TextView> (Resource.Id.POID_storyline);
			poiTitle = root.FindViewById<TextView> (Resource.Id.POID_title);
			poiPosition = root.FindViewById<TextView> (Resource.Id.POID_rank);
			poiDescription = root.FindViewById<TextView> (Resource.Id.POID_Description);
			mediaType = root.FindViewById<TextView> (Resource.Id.POID_MediaType);
			mediaCaption = root.FindViewById<TextView> (Resource.Id.POID_MediaCaption);
			closeButton = root.FindViewById<Button> (Resource.Id.POID_DialogBtnClose);
			nextButton = root.FindViewById<Button> (Resource.Id.POID_DialogBtnNext);
			playButton = root.FindViewById<Button> (Resource.Id.POID_DialogBtnPlay);

			tourLinearLayout = root.FindViewById<LinearLayout> (Resource.Id.POID_TourLinearLayout);
			positionLinearLayout = root.FindViewById<LinearLayout> (Resource.Id.POID_PositionLinearLayout);
			mediaLinearLayout = root.FindViewById<LinearLayout> (Resource.Id.POID_MediaLinearLayout);


			DetermineFRorEN ();

			if (PositionedUser.GetInstance ().activeStoryline == null) {
				tourLinearLayout.Visibility = ViewStates.Gone;
				positionLinearLayout.Visibility = ViewStates.Gone;
				nextButton.Visibility = ViewStates.Gone;
			} else {
				storyline = PositionedUser.GetInstance ().activeStoryline.storyline;
				InitializeStoryLineTitle ();
			}

			InitializePOIDescription ();
			InitializePOITitle ();
			DisplayMediaInformation ();

			closeButton.Click += (object sender, EventArgs e) => {
				beaconManger.StartRanging (RangingManager.region);
				Dismiss ();
			};

			nextButton.Click += (object sender, EventArgs e) =>
			{
				PositionedUser.GetInstance().activeStoryline.UpdatePoiStates();
				beaconManger.StartRanging (RangingManager.region);
				Dismiss();
			};

			playButton.Click +=	(object sender, EventArgs e) => 
			{

				var videoActivity = new Intent(Activity,typeof(MediaActivity));
				videoActivity.PutExtra ("VideoName", mediaCaption.Text);
				videoActivity.AddFlags(ActivityFlags.SingleTop);
				StartActivity(videoActivity);
			};
				
			return root;
		}

		void DetermineFRorEN ()
		{
			if (locale.ToString ().ToUpper ().Contains ("EN")) {
				language = "EN";
			}
		}

		void InitializeStoryLineTitle ()
		{
			foreach (Title3 t in storyline.title) {
				if (t.language == language) {
					storylineTitle.Text = t.title;
				}
			}
		}

		void InitializePOITitle ()
		{
			foreach (Title t in poi.title) {
				if (t.language == language) {
					poiTitle.Text = t.title;
				}
			}
		}

		void InitializePOIDescription ()
		{
			foreach (Description d in poi.description) {
				if (d.language == language) {
					poiDescription.Text = d.description;
				}
			}
		}

		//This assumes that a POI can only have ONE piece of media
		//Only available for videos now
		void DisplayMediaInformation()
		{
			if (poi.media.video != null) {
				mediaType.Text = "Video";
				mediaCaption.Text = poi.media.video [0].caption;
				mediaPath = poi.media.video [0].path;
			} else {
				mediaLinearLayout.Visibility = ViewStates.Gone;
			}
		}
			
	}
}
