﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Doppler.Droid
{
	public class DialogExitStoryline : DialogFragment
	{
		Button exitButtonYes;
		Button exitButtonNo;

		public DialogExitStoryline ()
		{
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);

			View root = inflater.Inflate (Resource.Layout.dialog_confirmExitStoryline, container, false);

			exitButtonYes = root.FindViewById<Button> (Resource.Id.SLDialogBtnYes);
			exitButtonNo = root.FindViewById<Button> (Resource.Id.SLDialogBtnNo);

			exitButtonNo.Click += (object sender, EventArgs e) => {
				Dismiss ();
			};

			exitButtonYes.Click += (object sender, EventArgs e) => {

				PositionedUser.GetInstance().activeStoryline = null;
				MainActivity main = (MainActivity) Activity;
				main.popViewStoryline();
				Dismiss ();
			};

			return root;
		}
	}
}

