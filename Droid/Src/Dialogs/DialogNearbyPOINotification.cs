﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Media;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using DroidShared;
using EstimoteSdk;

namespace Doppler.Droid
{
	public class DialogNearbyPOINotification : DialogFragment
	{

		Button okButton;
		Button viewButton;
		readonly Poi poi;
		TextView poiTitle;
		MediaPlayer mPlayer;
		BeaconManager beaconManager;
		readonly bool triggerFeedback;

		public DialogNearbyPOINotification(Poi poi, BeaconManager bm, bool trigger)
		{
			this.poi = poi;
			this.beaconManager = bm;
			this.triggerFeedback = trigger;
		}


		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			if (triggerFeedback) {
				Vibrator vibrator = (Vibrator)Activity.GetSystemService (Context.VibratorService);
				vibrator.Vibrate (1500);
				mPlayer = MediaPlayer.Create (this.Activity, Resource.Raw.sound);
				mPlayer.Start ();
			}
			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);
			var view = inflater.Inflate (Resource.Layout.dialog_nearbyPoiNotification, container, false);

			okButton = view.FindViewById<Button> (Resource.Id.POIDialogBtnOk);
			viewButton = view.FindViewById<Button> (Resource.Id.POIDialogBtnView);
			poiTitle = view.FindViewById<TextView> (Resource.Id.POIDialogPOIid);

			poiTitle.Text = poi.getID ();

			okButton.Click += (object sender, EventArgs e) => 
			{
				beaconManager.StartRanging(RangingManager.region);
				Dismiss();
			};

			viewButton.Click += (object sender, EventArgs e) => 
			{
				Dismiss();
				FragmentTransaction transaction = FragmentManager.BeginTransaction ();
				DialogPOI_View dialog = new DialogPOI_View(poi, beaconManager);
				dialog.SetStyle (DialogFragmentStyle.NoTitle, 0);
				dialog.Show (transaction,"dialog fragment");
			};

			return view;

		}


	}
}

