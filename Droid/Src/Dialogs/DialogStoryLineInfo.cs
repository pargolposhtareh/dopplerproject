﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using DroidShared;

namespace Doppler.Droid
{
	public class DialogStoryLineInfo : DialogFragment
	{
		readonly Storyline storyline;
		readonly int listViewItemPosition;
		TextView title;
		TextView walkingDuration;
		TextView floors;
		TextView description;
		Button closeButton;
		Button startButton;
		Button previewButton;

		public DialogStoryLineInfo(Storyline storyline, int listViewItemPosition)
		{
			this.storyline = storyline;
			this.listViewItemPosition = listViewItemPosition;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);

			View root = inflater.Inflate (Resource.Layout.dialog_storyLineInfo, container, false);

			title = root.FindViewById<TextView> (Resource.Id.SLDialogTitle);
			walkingDuration = root.FindViewById<TextView> (Resource.Id.SLDialogDurationValue);
			floors = root.FindViewById<TextView> (Resource.Id.SLDialogFloorsValue);
			description = root.FindViewById<TextView> (Resource.Id.SLDialogDescription);
			closeButton = root.FindViewById<Button> (Resource.Id.SLDialogBtnClose);
			startButton = root.FindViewById<Button> (Resource.Id.SLDialogBtnStart);
			previewButton = root.FindViewById<Button> (Resource.Id.SLDialogBtnPreview);

			title.Text = storyline.title.ElementAt(0).title;
			walkingDuration.Text = storyline.walkingTimeInMinutes + " min";
			floors.Text = storyline.floorsCovered;
			description.Text = storyline.description.ElementAt(0).description;

			closeButton.Click += (object sender, EventArgs e) => 
			Dismiss ();

			startButton.Click += (object sender, EventArgs e) =>
			{
				Dismiss();

				// starting a storyline based on the item position in the listview
				Activity.FragmentManager.BeginTransaction().Replace(Resource.Id.fragment_container, new ViewStoryline(listViewItemPosition)).AddToBackStack (null).Commit ();
			};

			previewButton.Click += (object sender, EventArgs e) => 
			{
				Dismiss();
				Activity.FragmentManager.BeginTransaction().Replace(Resource.Id.fragment_container, new ViewStoryline(listViewItemPosition)).AddToBackStack (null).Commit ();

			};

			return root;

		}
	}
}
