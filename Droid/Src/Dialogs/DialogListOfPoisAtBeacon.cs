﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using DroidShared;

namespace Doppler.Droid
{
	public class DialogListOfPoisAtBeacon : DialogFragment
	{
		readonly List<Poi> poiList;
		Button closeButton;
		readonly ViewUnguided view;

		public DialogListOfPoisAtBeacon (List<Poi> poiList, ViewUnguided view)
		{
			this.poiList = poiList;
			this.view = view;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View root = inflater.Inflate (Resource.Layout.dialog_ListOfPoisAtBeacon, container, false);
			closeButton = root.FindViewById<Button> (Resource.Id.ListOfPoisCloseButton);

			ListView mListView = root.FindViewById<ListView> (Resource.Id.PoisAtBeacon_listview);
			mListView.Focusable = false;

			PoiListAdapter adapter = new PoiListAdapter (this.Activity, poiList, this.Dialog, view);
			mListView.Adapter = adapter;


			closeButton.Click += (object sender, EventArgs e) => 
			{
				this.Dismiss();
			};
				
			return root;

		}
	}
}

