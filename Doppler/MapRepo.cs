﻿using System;
using System.Collections.Generic;

namespace Doppler.Droid
{
	
	public class MapRepo
	{
		public MapRepo(){}

		public List<string[]> allPaths = new List<string[]>();

		Dictionary<string, Coordinate> PointCoordinates = new Dictionary<string, Coordinate>();


		string[] path1 = new string[] {"start","c0","c1","c2","c3","c4","c5"};
		string[] path2 = new string[] {"start","c0","c1","c2","c3","c4","c6","c7","c8"};
		string[] path3 = new string[] {"start","c0","c17","c16","c15","c6","c7","c8"};


		public List<string[]> getAllPaths()
		{
			

			if(allPaths.Count != 0)
			{
				//Do Nothing
			}
			else
			{
				allPaths.Add(path1);
				allPaths.Add(path2);
				allPaths.Add(path3);
			}

			return allPaths;

		}

		public Dictionary<string, Coordinate> getAllPoints()
		{
			if(PointCoordinates.Count != 0)
			{
				//Do Nothing
			}
			else
			{
				PointCoordinates.Add("start" , new Coordinate (74.959,-61.87));
				PointCoordinates.Add("c0" , new Coordinate (74.77,-42.185));
				PointCoordinates.Add("c1" , new Coordinate (80.76,-43.593));
				PointCoordinates.Add("c2" , new Coordinate (80.76,14.06));
				PointCoordinates.Add("c3" , new Coordinate (74.019,14.76));
				PointCoordinates.Add("c4" , new Coordinate (40.01,13.359));
				PointCoordinates.Add("c5" , new Coordinate (40.01,46.406));
				PointCoordinates.Add("c6" , new Coordinate (-3.51,12.656));
				PointCoordinates.Add("c7" , new Coordinate (-21.94,16.171));
				PointCoordinates.Add("c8" , new Coordinate (-21.94,30.937));
				PointCoordinates.Add("c17" , new Coordinate (58.077,-42.187));
				PointCoordinates.Add("c16" , new Coordinate (27.684,-42.187));
				PointCoordinates.Add("c15" , new Coordinate (-1.4061,-42.187));
			}



			return PointCoordinates;

		}
	}
}

