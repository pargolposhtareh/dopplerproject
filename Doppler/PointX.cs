﻿using System;
using System.Collections.Generic;

namespace Doppler.Droid
{
		
	public class PointX 
	{

		public PointX(){}

		MapRepo MR = new MapRepo();

		public double distanceAB(Dictionary<string, Coordinate> PointCoordinates,string A , string B)
		{

			Coordinate pt1 = null;
			Coordinate pt2 = null;

			if (A == null || B == null || PointCoordinates == null || PointCoordinates.Count == 0)
				return 0;

			if(PointCoordinates.ContainsKey(A))
			{
				pt1 = PointCoordinates[A];
			}

			if(PointCoordinates.ContainsKey(B))
			{
				pt2 = PointCoordinates[B];
			}

			if (pt1 == null || pt2 == null) {
				return 0;
			} else {
				double distance = Math.Sqrt ((pt1.getXcoordinate () - pt2.getXcoordinate ()) * (pt1.getXcoordinate () - pt2.getXcoordinate ()) + (pt1.getYcoordinate () - pt2.getYcoordinate ()) * (pt1.getYcoordinate () - pt2.getYcoordinate ()));
				return distance;
			}

		}

		public string[] getShortestPath(List<string[]> paths)
		{
			List<double> pathSizes = new List<double> ();

			Dictionary<string, Coordinate> PointCoordinates = MR.getAllPoints ();

			foreach(string[] path in paths)
			{
				double size = 0;

				for(int i=0 ; i<(paths.Count) ; i++)
				{
					size = size + distanceAB (PointCoordinates,path[i],path[i+1]);
				}

				pathSizes.Add (size);
			}

			int z = 0;

			for(int i = 0; i < pathSizes.Count; i++) 
			{
				if (pathSizes [z] < pathSizes [i]) 
				{
					//Do Nothing
				} 
				else 
				{
					z = i;
				}
			}

			return paths [z];


		}

		public string getPathCoordinates(string[] shortestPath)
		{
			Dictionary<string, Coordinate> PointCoordinates = MR.getAllPoints (); 

			Coordinate point = null;

			string coor = "";

			foreach(string s in shortestPath)
			{
				if(PointCoordinates.ContainsKey(s))
				{
					point = PointCoordinates[s];

					double xC = point.getXcoordinate ();

					double yC = point.getYcoordinate ();

					coor= coor+ (xC.ToString()) +":"+ (yC.ToString()+":");

				}
			}

			return coor;
		}



	}
}

