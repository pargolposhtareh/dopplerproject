﻿using System;

namespace Doppler.Droid
{
	public class Coordinate 
	{

		public double x_cor;
		public double y_cor;

		public Coordinate(double x , double y)
		{
			x_cor = x;
			y_cor = y;
		}

		public double getXcoordinate()
		{
			return x_cor;
		}

		public void setXcoordinate(double x)
		{
			x_cor = x;
		}

		public double getYcoordinate()
		{
			return y_cor;
		}

		public void setYcoordinate(double y)
		{
			y_cor = y;
		}

	}	

}

