﻿using System;

namespace Doppler.Droid
{
	public class User
	{
		public string x;
		public string y;
		public string floor;

		public static User instance = null;

		private User ()
		{
			
		}

		public void setCoord(string x, string y)
		{
			this.x = x;
			this.y = y;
		}

		//singleton
		public static User GetInstance() {
			if (instance == null) {
				instance = new User ();
			}
			return instance;
		}

		public string getX() {

			return x;

		}
		public string getY() {

			return y;

		}
	}
}

