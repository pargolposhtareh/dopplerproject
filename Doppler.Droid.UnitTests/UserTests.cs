﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using DroidShared;

namespace Doppler.Droid.UnitTests
{
	[TestFixture]
	public class UserTests
	{
		[Test]
		public void testIfNull ()
		{
			User user = new User ();
			Poi p = new Poi ();
			p.x = "-10";
			p.y = "-15";
			p.id = "hello_world";
			p.floorID = "4";

			Assert.NotNull (user);
		}

		[Test]
		public void testUserWithValidPoi ()
		{
			User user = new User ();
			Poi p = new Poi ();
			p.x = "-10";
			p.y = "-15";
			p.id = "hello_world";
			p.floorID = "4";
			user.setPositionWithVertex (p);

			Assert.AreEqual ("-10", user.getVertex().getX());
			Assert.AreEqual ("-15", user.getVertex().getY());
			Assert.AreEqual ("hello_world", user.getVertex().getID());
			Assert.AreEqual ("4", user.getVertex().getFloorID());
		}

		[Test]
		public void testUserWithNullPoi ()
		{
			User user = new User ();

			Assert.IsNull (user.getVertex ());

			Poi p = null;
			user.setPositionWithVertex (p);

			Assert.IsNull (user.getVertex ());
		}

		[Test]
		public void testUserWithEmptyPoi ()
		{
			User user = new User ();
			Poi p = new Poi ();

			user.setPositionWithVertex (p);

			Assert.IsNull (null, p.getX());
			Assert.IsNull (null, p.getY());
			Assert.IsNull (null, p.getID());
			Assert.IsNull (null, p.getFloorID());
		}
	}
}

