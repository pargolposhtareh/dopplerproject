﻿using System;
using NUnit.Framework;
using DroidShared;
using System.Collections.Generic;

namespace Doppler.Droid.UnitTests
{
	[TestFixture]
	public class VertexDictionaryTests
	{

		[SetUp] 
		public void Init()
		{
			VertexDictionary v1 = VertexDictionary.GetInstance ();
			v1.vertexDictionary.Clear ();
			v1 = null;
		}


		[Test]
		public void TestGetSingleton()
		{
			VertexDictionary v1 = VertexDictionary.GetInstance ();
			VertexDictionary v2 = VertexDictionary.GetInstance ();

			Assert.AreSame (v1, v2);
		}

		[Test]
		public void TestGetVertexAtId()
		{
			VertexDictionary v1 = VertexDictionary.GetInstance ();
			Poi p1 = new Poi ();
			p1.id = "A1";

			v1.vertexDictionary.Add (p1.getID (), p1);

			Assert.AreSame (v1.GetVertexAtId ("A1"), p1);
		}

		[Test]
		public void TestGenerateDictionary()
		{
			Poi poi1 = new Poi ();
			poi1.id = "POI 1";
			Pot pot1 = new Pot ();
			pot1.id = "POT 1";
			Pot pot2 = new Pot ();
			pot2.id = "POT 2";

			List<Poi> pois = new List<Poi> ();
			List<Pot> pots = new List<Pot> ();

			pois.Add (poi1);
			pots.Add (pot1);
			pots.Add (pot2);

			VertexDictionary v1 = VertexDictionary.GetInstance ();
			v1.generateDictionary (pois, pots);

			Assert.AreSame(v1.GetVertexAtId("POI 1"), poi1);
			Assert.AreSame(v1.GetVertexAtId("POT 1"), pot1);
		}

		[Test]
		public void TestGenerateDictionaryEmptyLists()
		{
			List<Poi> pois = new List<Poi>();
			List<Pot> pots = new List<Pot>();

			VertexDictionary v1 = VertexDictionary.GetInstance ();
			v1.generateDictionary (pois, pots);

			Assert.IsEmpty (pois);
			Assert.IsEmpty (pots);
			Assert.True (v1.vertexDictionary.Count == 0);
		}

		[Test]
		public void TestListOfIdsToListOfVertex()
		{
			Poi poi1 = new Poi ();
			poi1.id = "POI 1";
			Pot pot1 = new Pot ();
			pot1.id = "POT 1";
			Pot pot2 = new Pot ();
			pot2.id = "POT 2";

			List<Poi> pois = new List<Poi> ();
			List<Pot> pots = new List<Pot> ();

			pois.Add (poi1);
			pots.Add (pot1);
			pots.Add (pot2);

			VertexDictionary v1 = VertexDictionary.GetInstance ();
			v1.generateDictionary (pois, pots);

			List<String> listString = new List<String> ();
			listString.Add ("POI 1");
			listString.Add ("POT 1");
			listString.Add ("POT 2");

			List<Vertex> listTest = v1.ListOfIdToListOfVertex (listString);

			Assert.AreSame (listTest [0], poi1);
			Assert.AreSame (listTest [1], pot1);
			Assert.AreSame (listTest [2], pot2);
		}
	}
}

