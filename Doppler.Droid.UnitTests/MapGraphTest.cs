﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using DroidShared;

namespace Doppler.Droid.UnitTests
{
	[TestFixture]
	public class MapGraphTest
	{
		MapGraph m1;

		[SetUp]
		public void init()
		{
			m1 = MapGraph.GetInstance ();
		}

		[Test]
		public void testIfSingleton ()
		{
			MapGraph m2 = MapGraph.GetInstance ();

			Assert.AreSame (m1, m2);
		}

		[Test]
		public void testIfMapGraphIsNull ()
		{
			Assert.IsNotNull (m1);
		}

		[Test]
		public void testDistanceIfNullParameters ()
		{
			double distance = m1.FindDistance (null, null);

			Assert.AreEqual (0, distance);
		}

		[Test]
		public void testDistanceIfStartVertexIsNull ()
		{
			Poi start = new Poi ();
			start.x = "-10";
			start.y = "-10";
			start.id = "startVertex";
			start.floorID = "2";

			double distance = m1.FindDistance (start, null);

			Assert.AreEqual (0, distance);
		}

		[Test]
		public void testDistanceIfEndVertexIsNull ()
		{
			MapGraph m1 = MapGraph.GetInstance ();
			Poi end = new Poi ();
			end.x = "-10";
			end.y = "-10";
			end.id = "endVertex";
			end.floorID = "2";

			double distance = m1.FindDistance (null, end);

			Assert.AreEqual (0, distance);
		}

		[Test]
		public void testDistanceIfBothVertexAreValid ()
		{
			Pot start = new Pot ();
			start.x = "-10";
			start.y = "-10";
			start.id = "startVertex";
			start.floorID = "2";

			Poi end = new Poi ();
			end.x = "20";
			end.y = "20";
			end.id = "endVertex";
			end.floorID = "2";

			double distance = m1.FindDistance (start, end);
			double distanceExpected =  Math.Sqrt (Math.Pow(((-10) - (20)),2) + Math.Pow(((-10) - (20)),2));

			Assert.AreEqual (distanceExpected, distance);
		}

	}
}

