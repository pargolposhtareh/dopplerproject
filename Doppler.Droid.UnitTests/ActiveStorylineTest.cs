﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using DroidShared;

namespace Doppler.Droid.UnitTests
{
	[TestFixture]
	public class ActiveStorylineTest
	{
		ActiveStoryline asl;
		List<string> path;
		List<Vertex> vertexPath;
		Storyline sl;
		VertexDictionary vd;

		[SetUp]
		public void init()
		{
			path = new List<string> ();
			path.Add ("poi A"); 
			path.Add ("pot B");
			path.Add ("pot C");
			path.Add ("poi D");
			path.Add ("pot E");
			path.Add ("pot F");
			path.Add ("poi G");

			sl = new Storyline ();
			sl.path = path;
		
			asl = new ActiveStoryline (sl);

			Poi a = new Poi ();
			Pot b = new Pot ();
			Pot c = new Pot ();
			Poi d = new Poi ();
			Pot e = new Pot ();
			Pot f = new Pot ();
			Poi g = new Poi ();

			a.id = "poi A";
			b.id = "pot B";
			c.id = "pot C";
			d.id = "poi D";
			e.id = "pot E";
			f.id = "pot F";
			g.id = "poi G";

			vd = VertexDictionary.GetInstance ();

			vd.vertexDictionary.Add (a.getID (), a);
			vd.vertexDictionary.Add (b.getID (), b);
			vd.vertexDictionary.Add (c.getID (), c);
			vd.vertexDictionary.Add (d.getID (), d);
			vd.vertexDictionary.Add (e.getID (), e);
			vd.vertexDictionary.Add (f.getID (), f);
			vd.vertexDictionary.Add (g.getID (), g);

			vertexPath = vd.ListOfIdToListOfVertex (path);
			asl.GenerateUnvisitedPOIs (vertexPath);
		}

		[TearDown]
		public void Dispose()
		{
			path = null;
			sl = null;
			vd.vertexDictionary.Clear();
			asl.unvisitedPOIs.Clear ();
		}

		[Test]
		public void TestConstructor()
		{
			Assert.AreSame (asl.storyline, sl);
			Assert.AreSame (asl.untraversedPoints, path);
		}

		[Test]
		public void TestGenerateUnvisitedPOIs()
		{
			Assert.AreEqual (asl.unvisitedPOIs [0].getID(), "poi A");
			Assert.AreEqual (asl.unvisitedPOIs [1].getID(), "poi D");
			Assert.AreEqual (asl.unvisitedPOIs [2].getID(), "poi G");
		}


		[Test]
		public void TestGetUnvisitedPOIs()
		{
			Assert.AreEqual (asl.GetCurrentUnvisitedPOI ().getID(), "poi A");
		}

		[Test]
		public void TestUpdatePointStates()
		{
			Assert.IsEmpty (asl.traversedPoints);
			Assert.IsEmpty (asl.visitedPOIs);
			Assert.IsTrue (asl.unvisitedPOIs.Count == 3);
			asl.UpdatePoiStates ();
		
			Assert.AreEqual (asl.traversedPoints [0], "poi A");
			Assert.AreEqual (asl.untraversedPoints [0], "pot B");
			Assert.AreEqual (asl.untraversedPoints [1], "pot C");
			Assert.AreEqual (asl.untraversedPoints [2], "poi D");

			asl.UpdatePoiStates ();

			Assert.AreEqual (asl.traversedPoints [0], "poi A");
			Assert.AreEqual (asl.traversedPoints [1], "pot B");
			Assert.AreEqual (asl.traversedPoints [2], "pot C");
			Assert.AreEqual (asl.traversedPoints [3], "poi D");
			Assert.AreEqual (asl.untraversedPoints [0], "pot E");
			Assert.AreEqual (asl.untraversedPoints [1], "pot F");
			Assert.AreEqual (asl.untraversedPoints [2], "poi G");

			asl.UpdatePoiStates ();

			Assert.AreEqual (asl.traversedPoints [0], "poi A");
			Assert.AreEqual (asl.traversedPoints [1], "pot B");
			Assert.AreEqual (asl.traversedPoints [2], "pot C");
			Assert.AreEqual (asl.traversedPoints [3], "poi D");
			Assert.AreEqual (asl.traversedPoints [4], "pot E");
			Assert.AreEqual (asl.traversedPoints [5], "pot F");
			Assert.AreEqual (asl.traversedPoints [6], "poi G");
		

		}


	}
}

